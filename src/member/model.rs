use crate::util::{OrganizationId, UserId, Version};
use chrono::prelude::*;
use dynomite::Attribute;
use serde::Serialize;

#[derive(Debug, Serialize)]
pub struct Member {
    #[serde(rename = "userId")]
    pub user_id: UserId,
    #[serde(rename = "organizationId")]
    pub organization_id: OrganizationId,
    #[serde(rename = "displayName")]
    pub display_name: String,
    pub state: MemberState,
    pub role: MemberRole,
    pub version: Version,
    #[serde(rename = "updatedAt")]
    pub updated_at: DateTime<Utc>,
    #[serde(rename = "createdAt")]
    pub created_at: DateTime<Utc>,
}

#[derive(Debug, Attribute, Clone, Copy, Serialize)]
pub enum MemberState {
    Active,
    // Disabled,
}

#[derive(Debug, Attribute, Clone, Copy, Serialize)]
pub enum MemberRole {
    // Admin,
    // Moderator,
    User,
}
