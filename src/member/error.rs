use crate::organization::OrganizationError;
use crate::util::Version;
use dynomite::AttributeError;
use failure::Fail;
use lambda_runtime::error::LambdaErrorExt;

#[derive(Debug, Fail)]
pub enum MemberError {
    #[fail(display = "general member error: {}", _0)]
    GeneralMemberError(Box<dyn std::error::Error + Sync + Send>),
    #[fail(display = "persistence member error: {}", _0)]
    PersistenceMemberError(AttributeError),
    #[fail(display = "member not found error: {}", _0)]
    MemberNotFoundError(String),
    #[fail(display = "organization not found error: {}", _0)]
    MemberOrganizationNotFoundError(String),
    #[fail(display = "member version outdated: {}", _0)]
    MemberVersionOutdatedError(Version),
}

impl From<OrganizationError> for MemberError {
    fn from(organization_error: OrganizationError) -> Self {
        match organization_error {
            OrganizationError::OrganizationNotFoundError(organization) => {
                MemberError::MemberOrganizationNotFoundError(organization)
            }
            other => MemberError::GeneralMemberError(Box::new(other.compat())),
        }
    }
}

impl LambdaErrorExt for MemberError {
    fn error_type(&self) -> &str {
        self.name().unwrap_or_else(|| "GeneralError")
    }
}
