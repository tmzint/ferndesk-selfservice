use crate::dynamo::DateTimeUtc;
use crate::member::error::MemberError;
use crate::member::model::MemberState;
use crate::member::{Member, MemberRole};
use crate::util::{OrganizationId, UserId, Version};
use chrono::prelude::*;
use dynomite::{
    dynamodb::{AttributeValue, DynamoDb, PutItemInput},
    Item,
};
use maplit::hashmap;
use slog_scope::{error, info};
use tokio::prelude::Future;

#[derive(Debug)]
pub struct InsertMember {
    pub user_id: UserId,
    pub organization_id: OrganizationId,
    pub display_name: String,
    pub role: MemberRole,
}

#[derive(Item, Debug, Clone)]
struct MemberDynamoItem {
    #[dynomite(partition_key)]
    pk: String,
    #[dynomite(sort_key)]
    sk: String,
    #[dynomite(rename = "userId")]
    user_id: UserId,
    #[dynomite(rename = "organizationId")]
    organization_id: OrganizationId,
    #[dynomite(rename = "displayName")]
    display_name: String,
    state: MemberState,
    role: MemberRole,
    version: Version,
    #[dynomite(rename = "updatedAt")]
    updated_at: DateTimeUtc,
    #[dynomite(rename = "createdAt")]
    created_at: DateTimeUtc,
}

impl Into<Member> for MemberDynamoItem {
    fn into(self) -> Member {
        Member {
            user_id: self.user_id,
            organization_id: self.organization_id,
            display_name: self.display_name,
            state: self.state,
            role: self.role,
            version: self.version,
            updated_at: self.updated_at.into(),
            created_at: self.created_at.into(),
        }
    }
}

impl From<Member> for MemberDynamoItem {
    fn from(member: Member) -> Self {
        MemberDynamoItem {
            pk: add_partition_key_prefix(&member.organization_id),
            sk: member.organization_id.0.clone(),
            user_id: member.user_id,
            organization_id: member.organization_id,
            display_name: member.display_name,
            state: member.state,
            role: member.role,
            version: member.version,
            updated_at: member.updated_at.into(),
            created_at: member.created_at.into(),
        }
    }
}

pub struct MemberRepository<T> {
    table_name: String,
    dynamo_client: T,
}

impl<T: DynamoDb> MemberRepository<T> {
    pub fn new(table_name: String, dynamo_client: T) -> Self {
        MemberRepository {
            table_name,
            dynamo_client,
        }
    }

    pub fn insert_member(
        &self,
        insert_member: InsertMember,
    ) -> impl Future<Item = Member, Error = MemberError> + Send {
        info!("trying to insert member {:?}", insert_member);

        let created_at = Utc::now();
        let pk = add_partition_key_prefix(&insert_member.organization_id);
        let member_dynamo_item = MemberDynamoItem {
            pk,
            sk: insert_member.user_id.0.clone(),
            user_id: insert_member.user_id.clone(),
            organization_id: insert_member.organization_id.clone(),
            display_name: insert_member.display_name.clone(),
            state: MemberState::Active,
            role: insert_member.role,
            version: Version::default(),
            updated_at: created_at.clone().into(),
            created_at: created_at.into(),
        };

        let input = PutItemInput {
            table_name: self.table_name.clone(),
            condition_expression: Some(
                "attribute_not_exists(pk) OR version = :v_version".to_owned(),
            ),
            expression_attribute_values: Some(hashmap! {
                ":v_version".to_owned() => AttributeValue { n: Some("0".to_owned()), ..AttributeValue::default()}
            }),
            item: member_dynamo_item.clone().into(),
            ..PutItemInput::default()
        };

        self.dynamo_client
            .put_item(input)
            .map(|_| member_dynamo_item.into())
            .map_err(move |e| {
                error!("insert member {:?} failed with {} ", insert_member, e);
                MemberError::GeneralMemberError(Box::new(e))
            })
    }
}

fn add_partition_key_prefix(organization_id: &OrganizationId) -> String {
    format!("Member#{}", organization_id.0)
}
