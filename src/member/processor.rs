use crate::member::error::MemberError;
use crate::member::repository::{InsertMember, MemberRepository};
use crate::member::{Member, MemberEventPublisher, MemberRole};
use crate::organization::OrganizationRepository;
use crate::util::OrganizationId;
use crate::util::UserId;
use dynomite::dynamodb::DynamoDb;
use enclose::enclose;
use rusoto_sns::Sns;
use std::sync::Arc;
use tokio::prelude::Future;

pub struct CreateMemberProcessor<DYNAMO, SNS> {
    member_repository: Arc<MemberRepository<DYNAMO>>,
    member_event_publisher: Arc<MemberEventPublisher<SNS>>,
    organization_repository: Arc<OrganizationRepository<DYNAMO>>,
}

impl<DYNAMO: DynamoDb + Send + Sync, SNS: Sns + Send + Sync> CreateMemberProcessor<DYNAMO, SNS> {
    pub fn new(
        member_repository: Arc<MemberRepository<DYNAMO>>,
        member_event_publisher: Arc<MemberEventPublisher<SNS>>,
        organization_repository: Arc<OrganizationRepository<DYNAMO>>,
    ) -> Self {
        CreateMemberProcessor {
            member_repository,
            member_event_publisher,
            organization_repository,
        }
    }

    pub fn create_member(
        &self,
        user_id: UserId,
        organization_id: OrganizationId,
        display_name: String,
    ) -> impl Future<Item = Member, Error = MemberError> + Send {
        //TODO: check for existing authorization of user and existing user
        let insert_member = InsertMember {
            user_id,
            organization_id: organization_id.clone(),
            display_name,
            role: MemberRole::User,
        };

        self.organization_repository
            .find_organization(organization_id)
            .from_err()
            .and_then(
                enclose!((self.member_repository => member_repository) move |_| {
                    member_repository.insert_member(insert_member)
                }),
            )
            .and_then(
                enclose!((self.member_event_publisher => member_event_publisher) move |member| {
                    member_event_publisher
                        .publish_member_created(&member)
                        .map(|_| member)
                }),
            )
    }
}
