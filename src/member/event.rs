use crate::member::model::MemberState;
use crate::member::{Member, MemberError, MemberRole};
use crate::sns::MessageId;
use crate::util::{OrganizationId, UserId, Version};
use chrono::prelude::*;
use maplit::hashmap;
use rusoto_sns::{MessageAttributeValue, PublishInput, Sns};
use serde::Serialize;
use slog_scope::{error, info};
use std::sync::Arc;
use tokio::prelude::{future, Future};

const VERSION: &str = "1.0";
const TYPE_MEMBER_CREATED: &str = "MemberCreated";

#[derive(Debug, Serialize)]
struct MemberCreated {
    #[serde(rename = "userId")]
    pub user_id: UserId,
    #[serde(rename = "organizationId")]
    pub organization_id: OrganizationId,
    #[serde(rename = "displayName")]
    pub display_name: String,
    pub state: MemberState,
    pub role: MemberRole,
    pub version: Version,
    pub timestamp: DateTime<Utc>,
}

pub struct MemberEventPublisher<T> {
    topic_arn: String,
    sns_client: T,
}

impl<T: Sns> MemberEventPublisher<T> {
    pub fn new(topic_arn: String, sns_client: T) -> Self {
        MemberEventPublisher {
            topic_arn,
            sns_client,
        }
    }

    pub fn publish_member_created(
        self: Arc<Self>,
        member: &Member,
    ) -> impl Future<Item = MessageId, Error = MemberError> {
        let member_created = MemberCreated {
            user_id: member.user_id.to_owned(),
            organization_id: member.organization_id.to_owned(),
            display_name: member.display_name.to_owned(),
            state: member.state,
            role: member.role,
            version: member.version,
            timestamp: member.created_at.to_owned(),
        };

        info!("trying to publish member created {:?}", member_created);

        let message = serde_json::to_string(&member_created)
            .map_err(|e| MemberError::GeneralMemberError(Box::new(e)));

        future::result(message).and_then(move |message| {
            let input = PublishInput {
                message_attributes: Some(hashmap! {
                    "version".to_owned() => MessageAttributeValue {
                        string_value: Some(VERSION.to_owned()),
                        data_type: "String".to_owned(),
                        ..Default::default()
                    },
                    "type".to_owned() => MessageAttributeValue {
                        string_value: Some(TYPE_MEMBER_CREATED.to_owned()),
                        data_type: "String".to_owned(),
                        ..Default::default()
                    },
                }),
                message,
                topic_arn: Some(self.topic_arn.clone()),
                ..Default::default()
            };

            self.sns_client
                .publish(input)
                .map_err(move |e| {
                    error!(
                        "publish member created {:?} failed with {} ",
                        member_created, e
                    );
                    MemberError::GeneralMemberError(Box::new(e))
                })
                .and_then(|result| {
                    result.message_id.map(MessageId).ok_or_else(|| {
                        MemberError::GeneralMemberError(Box::new(
                            failure::err_msg("message id missing").compat(),
                        ))
                    })
                })
        })
    }
}
