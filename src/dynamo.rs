use chrono::prelude::*;
use derive_more::{Deref, From, Into};
use dynomite::{dynamodb::AttributeValue, Attribute, AttributeError};

#[derive(Debug, Clone, From, Deref, Into)]
pub struct DateTimeUtc(pub DateTime<Utc>);

impl Attribute for DateTimeUtc {
    fn into_attr(self: Self) -> AttributeValue {
        AttributeValue {
            s: Some(self.0.to_rfc3339_opts(SecondsFormat::Millis, true)),
            ..AttributeValue::default()
        }
    }

    fn from_attr(value: AttributeValue) -> Result<Self, AttributeError> {
        value.s.ok_or(AttributeError::InvalidType).and_then(|s| {
            s.parse::<DateTime<Utc>>()
                .map(DateTimeUtc)
                .map_err(|_| AttributeError::InvalidFormat)
        })
    }
}

impl Into<String> for DateTimeUtc {
    fn into(self) -> String {
        self.0.to_rfc3339_opts(SecondsFormat::Millis, true)
    }
}