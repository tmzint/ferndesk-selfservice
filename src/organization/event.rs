use crate::organization::{Organization, OrganizationError, OrganizationVisibility};
use crate::sns::MessageId;
use crate::util::{OrganizationId, UserId, Version};
use chrono::prelude::*;
use maplit::hashmap;
use rusoto_sns::{MessageAttributeValue, PublishInput, Sns};
use serde::Serialize;
use slog_scope::{error, info};
use std::sync::Arc;
use tokio::prelude::{future, Future};

const VERSION: &str = "1.0";
const TYPE_ORGANIZATION_CREATED: &str = "OrganizationCreated";

#[derive(Debug, Serialize)]
struct OrganizationCreated {
    #[serde(rename = "organizationId")]
    pub organization_id: OrganizationId,
    pub name: String,
    #[serde(rename = "creatorId")]
    pub creator_id: UserId,
    pub visibility: OrganizationVisibility,
    pub version: Version,
    pub timestamp: DateTime<Utc>,
}

pub struct OrganizationEventPublisher<T> {
    topic_arn: String,
    sns_client: T,
}

impl<T: Sns> OrganizationEventPublisher<T> {
    pub fn new(topic_arn: String, sns_client: T) -> Self {
        OrganizationEventPublisher {
            topic_arn,
            sns_client,
        }
    }

    pub fn publish_organization_created(
        self: Arc<Self>,
        organization: &Organization,
    ) -> impl Future<Item = MessageId, Error = OrganizationError> {
        let organization_created = OrganizationCreated {
            organization_id: organization.organization_id.to_owned(),
            name: organization.name.to_owned(),
            creator_id: organization.creator_id.to_owned(),
            visibility: organization.visibility,
            version: organization.version,
            timestamp: organization.created_at.to_owned(),
        };

        info!(
            "trying to publish organization created {:?}",
            organization_created
        );

        let message = serde_json::to_string(&organization_created)
            .map_err(|e| OrganizationError::GeneralOrganizationError(Box::new(e)));

        future::result(message).and_then(move |message| {
            let input = PublishInput {
                message_attributes: Some(hashmap! {
                    "version".to_owned() => MessageAttributeValue {
                        string_value: Some(VERSION.to_owned()),
                        data_type: "String".to_owned(),
                        ..Default::default()
                    },
                    "type".to_owned() => MessageAttributeValue {
                        string_value: Some(TYPE_ORGANIZATION_CREATED.to_owned()),
                        data_type: "String".to_owned(),
                        ..Default::default()
                    },
                }),
                message,
                topic_arn: Some(self.topic_arn.clone()),
                ..Default::default()
            };

            self.sns_client
                .publish(input)
                .map_err(move |e| {
                    error!(
                        "publish organization created {:?} failed with {} ",
                        organization_created, e
                    );
                    OrganizationError::GeneralOrganizationError(Box::new(e))
                })
                .and_then(|result| {
                    result.message_id.map(MessageId).ok_or_else(|| {
                        OrganizationError::GeneralOrganizationError(Box::new(
                            failure::err_msg("message id missing").compat(),
                        ))
                    })
                })
        })
    }
}
