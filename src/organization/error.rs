use crate::util::Version;
use dynomite::AttributeError;
use failure::Fail;
use lambda_runtime::error::LambdaErrorExt;

#[derive(Debug, Fail)]
pub enum OrganizationError {
    #[fail(display = "general organization error: {}", _0)]
    GeneralOrganizationError(Box<dyn std::error::Error + Sync + Send>),
    #[fail(display = "persistence organization error: {}", _0)]
    PersistenceOrganizationError(AttributeError),
    #[fail(display = "organization not found error: {}", _0)]
    OrganizationNotFoundError(String),
    #[fail(display = "organization version outdated: {}", _0)]
    OrganizationVersionOutdatedError(Version),
}

impl LambdaErrorExt for OrganizationError {
    fn error_type(&self) -> &str {
        self.name().unwrap_or_else(|| "GeneralError")
    }
}
