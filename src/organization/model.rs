use crate::util::{OrganizationId, UserId, Version};
use chrono::prelude::*;
use dynomite::Attribute;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize)]
pub struct Organization {
    #[serde(rename = "organizationId")]
    pub organization_id: OrganizationId,
    pub name: String,
    #[serde(rename = "creatorId")]
    pub creator_id: UserId,
    pub visibility: OrganizationVisibility,
    pub version: Version,
    #[serde(rename = "updatedAt")]
    pub updated_at: DateTime<Utc>,
    #[serde(rename = "createdAt")]
    pub created_at: DateTime<Utc>,
}

#[derive(Debug, Attribute, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
pub enum OrganizationVisibility {
    Private,
}
