use crate::organization::error::OrganizationError;
use crate::organization::event::OrganizationEventPublisher;
use crate::organization::repository::{InsertOrganization, OrganizationRepository};
use crate::organization::{Organization, OrganizationVisibility};
use crate::util;
use crate::util::{OrganizationId, UserId};
use dynomite::dynamodb::DynamoDb;
use enclose::enclose;
use rusoto_sns::Sns;
use std::sync::Arc;
use tokio::prelude::Future;

pub struct CreateOrganizationProcessor<DYNAMO, SNS> {
    organization_repository: Arc<OrganizationRepository<DYNAMO>>,
    organization_event_publisher: Arc<OrganizationEventPublisher<SNS>>,
}

impl<DYNAMO: DynamoDb + Send + Sync, SNS: Sns + Send + Sync>
    CreateOrganizationProcessor<DYNAMO, SNS>
{
    pub fn new(
        organization_repository: Arc<OrganizationRepository<DYNAMO>>,
        organization_event_publisher: Arc<OrganizationEventPublisher<SNS>>,
    ) -> Self {
        CreateOrganizationProcessor {
            organization_repository,
            organization_event_publisher,
        }
    }

    pub fn create_organization(
        &self,
        name: String,
        creator_id: UserId,
        visibility: OrganizationVisibility,
    ) -> impl Future<Item = Organization, Error = OrganizationError> + Send {
        let organization_id = OrganizationId(util::generate_id());

        let insert_organization = InsertOrganization {
            organization_id,
            name,
            creator_id,
            visibility,
        };

        self.organization_repository
            .insert_organization(insert_organization)
            .and_then(enclose!((self.organization_event_publisher => organization_event_publisher) move |organization| {
                organization_event_publisher
                    .publish_organization_created(&organization)
                    .map(|_| organization)
            }))
    }
}
