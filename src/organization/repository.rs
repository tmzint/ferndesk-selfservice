use crate::dynamo::DateTimeUtc;
use crate::organization::error::OrganizationError;
use crate::organization::model::Organization;
use crate::organization::OrganizationVisibility;
use crate::util::{OrganizationId, UserId, Version};
use chrono::prelude::*;
use dynomite::{
    dynamodb::{AttributeValue, DynamoDb, GetItemInput, PutItemInput},
    FromAttributes, Item,
};
use enclose::enclose;
use maplit::hashmap;
use slog_scope::{error, info};
use tokio::prelude::Future;

const SORT_KEY_CONSTANT: &str = "0";

#[derive(Debug)]
pub struct InsertOrganization {
    pub organization_id: OrganizationId,
    pub name: String,
    pub creator_id: UserId,
    pub visibility: OrganizationVisibility,
}

#[derive(Item, Debug, Clone)]
struct OrganizationDynamoItem {
    #[dynomite(partition_key)]
    pk: String,
    #[dynomite(sort_key)]
    sk: String,
    #[dynomite(rename = "organizationId")]
    organization_id: OrganizationId,
    name: String,
    #[dynomite(rename = "creatorId")]
    creator_id: UserId,
    visibility: OrganizationVisibility,
    version: Version,
    #[dynomite(rename = "updatedAt")]
    updated_at: DateTimeUtc,
    #[dynomite(rename = "createdAt")]
    created_at: DateTimeUtc,
}

impl Into<Organization> for OrganizationDynamoItem {
    fn into(self) -> Organization {
        Organization {
            organization_id: self.organization_id,
            name: self.name,
            creator_id: self.creator_id,
            visibility: self.visibility,
            version: self.version,
            updated_at: self.updated_at.into(),
            created_at: self.created_at.into(),
        }
    }
}

impl From<Organization> for OrganizationDynamoItem {
    fn from(organization: Organization) -> Self {
        OrganizationDynamoItem {
            pk: add_partition_key_prefix(&organization.organization_id),
            sk: SORT_KEY_CONSTANT.to_owned(),
            organization_id: organization.organization_id,
            name: organization.name,
            creator_id: organization.creator_id,
            visibility: organization.visibility,
            version: organization.version,
            updated_at: organization.updated_at.into(),
            created_at: organization.created_at.into(),
        }
    }
}

pub struct OrganizationRepository<T> {
    table_name: String,
    dynamo_client: T,
}

impl<T: DynamoDb> OrganizationRepository<T> {
    pub fn new(table_name: String, dynamo_client: T) -> Self {
        OrganizationRepository {
            table_name,
            dynamo_client,
        }
    }

    pub fn find_organization(
        &self,
        id: OrganizationId,
    ) -> impl Future<Item = Organization, Error = OrganizationError> + Send {
        let pk = add_partition_key_prefix(&id);
        let input = GetItemInput {
            table_name: self.table_name.clone(),
            key: hashmap! {
                "pk".to_owned() => AttributeValue { s: Some(pk), ..AttributeValue::default()},
                "sk".to_owned() => AttributeValue { s: Some(SORT_KEY_CONSTANT.to_owned()), ..AttributeValue::default()}
            },
            ..GetItemInput::default()
        };

        self.dynamo_client
            .get_item(input)
            .map_err(enclose!((id) move |e| {
                error!("find organization from id {:?} failed with {} ", id, e);
                OrganizationError::GeneralOrganizationError(Box::new(e))
            }))
            .and_then(move |result| match result.item {
                Some(item) => OrganizationDynamoItem::from_attrs(item).map_err(|e| {
                    error!("find organization from id {:?} failed with {} ", id, e);
                    OrganizationError::PersistenceOrganizationError(e)
                }),
                None => {
                    info!("did not find organization from id {:?}", id);
                    Err(OrganizationError::OrganizationNotFoundError(
                        add_partition_key_prefix(&id),
                    ))
                }
            })
            .map(|organization_item| organization_item.into())
    }

    pub fn insert_organization(
        &self,
        insert_organization: InsertOrganization,
    ) -> impl Future<Item = Organization, Error = OrganizationError> + Send {
        info!("trying to insert organization {:?}", insert_organization);

        let created_at = Utc::now();
        let pk = add_partition_key_prefix(&insert_organization.organization_id);
        let organization_dynamo_item = OrganizationDynamoItem {
            pk,
            sk: SORT_KEY_CONSTANT.to_owned(),
            organization_id: insert_organization.organization_id.clone(),
            name: insert_organization.name.clone(),
            creator_id: insert_organization.creator_id.clone(),
            visibility: insert_organization.visibility,
            version: Version::default(),
            updated_at: created_at.clone().into(),
            created_at: created_at.into(),
        };

        let input = PutItemInput {
            table_name: self.table_name.clone(),
            condition_expression: Some(
                "attribute_not_exists(pk) OR (version = :v_version AND creator_id = :v_creator_id)"
                    .to_owned(),
            ),
            expression_attribute_values: Some(hashmap! {
                ":v_version".to_owned() => AttributeValue {
                    n: Some("0".to_owned()),
                    ..AttributeValue::default()
                },
                ":v_creator_id".to_owned() => AttributeValue {
                    s: Some(organization_dynamo_item.creator_id.0.clone()),
                    ..AttributeValue::default()
                },
            }),
            item: organization_dynamo_item.clone().into(),
            ..PutItemInput::default()
        };

        self.dynamo_client
            .put_item(input)
            .map(|_| organization_dynamo_item.into())
            .map_err(move |e| {
                error!(
                    "insert organization {:?} failed with {} ",
                    insert_organization, e
                );
                OrganizationError::GeneralOrganizationError(Box::new(e))
            })
    }
}

fn add_partition_key_prefix(organization_id: &OrganizationId) -> String {
    format!("Organization#{}", organization_id.0)
}
