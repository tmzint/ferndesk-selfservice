use derive_more::{Add, Deref, Display, From};
use dynomite::{dynamodb::AttributeValue, Attribute, AttributeError};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(PartialEq, Eq, Debug, Display, Serialize, Deserialize, From, Deref, Clone)]
pub struct UserId(pub String);

impl Attribute for UserId {
    fn into_attr(self: Self) -> AttributeValue {
        AttributeValue {
            s: Some(self.0),
            ..AttributeValue::default()
        }
    }

    fn from_attr(value: AttributeValue) -> Result<Self, AttributeError> {
        value.s.ok_or(AttributeError::InvalidType).map(UserId)
    }
}

#[derive(PartialEq, Eq, Debug, Display, Serialize, Deserialize, From, Deref, Clone)]
pub struct OrganizationId(pub String);

impl Attribute for OrganizationId {
    fn into_attr(self: Self) -> AttributeValue {
        AttributeValue {
            s: Some(self.0),
            ..AttributeValue::default()
        }
    }

    fn from_attr(value: AttributeValue) -> Result<Self, AttributeError> {
        value
            .s
            .ok_or(AttributeError::InvalidType)
            .map(OrganizationId)
    }
}

#[derive(
    PartialEq, Eq, Debug, Display, Serialize, Deserialize, From, Deref, Add, Clone, Copy, Default,
)]
pub struct Version(pub u32);

impl Attribute for Version {
    fn into_attr(self: Self) -> AttributeValue {
        AttributeValue {
            n: Some(self.0.to_string()),
            ..AttributeValue::default()
        }
    }

    fn from_attr(value: AttributeValue) -> Result<Self, AttributeError> {
        value.n.ok_or(AttributeError::InvalidType).and_then(|n| {
            n.parse::<u32>()
                .map(Version)
                .map_err(|_| AttributeError::InvalidFormat)
        })
    }
}

pub fn generate_id() -> String {
    base64::encode_config(Uuid::new_v4().as_bytes(), base64::URL_SAFE_NO_PAD)
}
