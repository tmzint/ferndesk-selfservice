use std::error::Error;

use dynomite::{
    dynamodb::{DynamoDb, DynamoDbClient},
    retry::{Policy, Retries},
};
use ferndesk_selfservice::member::{
    CreateMemberProcessor, Member, MemberEventPublisher, MemberRepository,
};
use ferndesk_selfservice::organization::OrganizationRepository;
use ferndesk_selfservice::util::{OrganizationId, UserId};
use lambda_runtime::{error::HandlerError, lambda, Context, Handler};
use rusoto_core::Region;
use rusoto_sns::{Sns, SnsClient};
use serde::Deserialize;
use slog::{o, Drain};
use std::sync::Arc;
use tokio::prelude::Future;
use tokio::runtime::Runtime;

#[derive(Deserialize, Debug)]
struct Config {
    selfservice_table_name: String,
    selfservice_sns_topic_arn: String,
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct MemberCreateRequest {
    #[serde(rename = "userId")]
    pub user_id: UserId,
    #[serde(rename = "organizationId")]
    pub organization_id: OrganizationId,
    #[serde(rename = "displayName")]
    pub display_name: String,
}

struct App<DYNAMO, SNS> {
    create_member_processor: CreateMemberProcessor<DYNAMO, SNS>,
    rt: Runtime,
}

impl<DYNAMO: DynamoDb + 'static + Send + Sync, SNS: Sns + 'static + Send + Sync>
    Handler<MemberCreateRequest, Member, HandlerError> for App<DYNAMO, SNS>
{
    fn run(
        &mut self,
        member_create: MemberCreateRequest,
        _ctx: Context,
    ) -> Result<Member, HandlerError> {
        let result = self
            .create_member_processor
            .create_member(
                member_create.user_id,
                member_create.organization_id,
                member_create.display_name,
            )
            .map_err(HandlerError::new)
            .from_err();

        self.rt.block_on(result)
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let config = envy::from_env::<Config>()?;

    let decorator = slog_term::PlainDecorator::new(std::io::stdout());
    let drain = slog_term::CompactFormat::new(decorator).build().fuse();
    let drain = slog_async::Async::new(drain).build().fuse();
    let log = slog::Logger::root(drain, o!());
    let _guard = slog_scope::set_global_logger(log);
    slog_stdlog::init()?;

    let lambda_rt = Runtime::new()?;
    let app_rt = Runtime::new()?;

    let app = create_app(config, app_rt);
    lambda!(app, lambda_rt);

    Ok(())
}

fn create_app(
    config: Config,
    rt: Runtime,
) -> impl Handler<MemberCreateRequest, Member, HandlerError> {
    let dynamo_client = DynamoDbClient::new(Region::EuWest1).with_retries(Policy::default());
    let organization_repository = Arc::new(OrganizationRepository::new(
        config.selfservice_table_name.clone(),
        dynamo_client.clone(),
    ));
    let member_repository = Arc::new(MemberRepository::new(
        config.selfservice_table_name,
        dynamo_client,
    ));
    let sns_client = SnsClient::new(Region::EuWest1);
    let member_event_publisher = Arc::new(MemberEventPublisher::new(
        config.selfservice_sns_topic_arn,
        sns_client,
    ));
    let create_member_processor = CreateMemberProcessor::new(
        member_repository,
        member_event_publisher,
        organization_repository,
    );

    App {
        create_member_processor,
        rt,
    }
}
