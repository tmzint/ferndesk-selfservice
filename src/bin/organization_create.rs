use std::error::Error;

use dynomite::{
    dynamodb::{DynamoDb, DynamoDbClient},
    retry::{Policy, Retries},
};
use ferndesk_selfservice::organization::{
    CreateOrganizationProcessor, Organization, OrganizationEventPublisher, OrganizationRepository,
    OrganizationVisibility,
};
use ferndesk_selfservice::util::UserId;
use lambda_runtime::{error::HandlerError, lambda, Context, Handler};
use rusoto_core::Region;
use rusoto_sns::{Sns, SnsClient};
use serde::Deserialize;
use slog::{o, Drain};
use std::sync::Arc;
use tokio::prelude::Future;
use tokio::runtime::Runtime;

#[derive(Deserialize, Debug)]
struct Config {
    selfservice_table_name: String,
    selfservice_sns_topic_arn: String,
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct OrganizationCreateRequest {
    pub name: String,
    #[serde(rename = "creatorId")]
    pub creator_id: UserId,
    pub visibility: OrganizationVisibility,
}

struct App<DYNAMO, SNS> {
    create_organization_processor: CreateOrganizationProcessor<DYNAMO, SNS>,
    rt: Runtime,
}

impl<DYNAMO: DynamoDb + 'static + Send + Sync, SNS: Sns + 'static + Send + Sync>
    Handler<OrganizationCreateRequest, Organization, HandlerError> for App<DYNAMO, SNS>
{
    fn run(
        &mut self,
        organization_create: OrganizationCreateRequest,
        _ctx: Context,
    ) -> Result<Organization, HandlerError> {
        let result = self
            .create_organization_processor
            .create_organization(
                organization_create.name,
                organization_create.creator_id,
                organization_create.visibility,
            )
            .map_err(HandlerError::new)
            .from_err();

        self.rt.block_on(result)
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let config = envy::from_env::<Config>()?;

    let decorator = slog_term::PlainDecorator::new(std::io::stdout());
    let drain = slog_term::CompactFormat::new(decorator).build().fuse();
    let drain = slog_async::Async::new(drain).build().fuse();
    let log = slog::Logger::root(drain, o!());
    let _guard = slog_scope::set_global_logger(log);
    slog_stdlog::init()?;

    let lambda_rt = Runtime::new()?;
    let app_rt = Runtime::new()?;

    let app = create_app(config, app_rt);
    lambda!(app, lambda_rt);

    Ok(())
}

fn create_app(
    config: Config,
    rt: Runtime,
) -> impl Handler<OrganizationCreateRequest, Organization, HandlerError> {
    let dynamo_client = DynamoDbClient::new(Region::EuWest1).with_retries(Policy::default());
    let organization_repository = Arc::new(OrganizationRepository::new(
        config.selfservice_table_name,
        dynamo_client,
    ));
    let sns_client = SnsClient::new(Region::EuWest1);
    let organization_event_publisher = Arc::new(OrganizationEventPublisher::new(
        config.selfservice_sns_topic_arn,
        sns_client,
    ));
    let create_organization_processor =
        CreateOrganizationProcessor::new(organization_repository, organization_event_publisher);

    App {
        create_organization_processor,
        rt,
    }
}
