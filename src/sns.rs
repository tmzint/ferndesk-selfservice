use derive_more::{Deref, Display, From};
use dynomite::{dynamodb::AttributeValue, Attribute, AttributeError};
use serde::{Deserialize, Serialize};

#[derive(PartialEq, Eq, Debug, Display, Serialize, Deserialize, From, Deref, Clone)]
pub struct MessageId(pub String);

impl Attribute for MessageId {
    fn into_attr(self: Self) -> AttributeValue {
        AttributeValue {
            n: Some(self.0),
            ..AttributeValue::default()
        }
    }

    fn from_attr(value: AttributeValue) -> Result<Self, AttributeError> {
        value.s.ok_or(AttributeError::InvalidType).map(MessageId)
    }
}
