#!/bin/bash

STAGE=$1

function packageLambda() {
  ARTEFACT=$1
  mkdir -p "./target/x86_64-unknown-linux-musl/release/lambda/$ARTEFACT" && \
    cp "./target/x86_64-unknown-linux-musl/release/$ARTEFACT" "./target/x86_64-unknown-linux-musl/release/lambda/$ARTEFACT/bootstrap" && \
    strip "./target/x86_64-unknown-linux-musl/release/lambda/$ARTEFACT/bootstrap" && \
    zip -j "./target/x86_64-unknown-linux-musl/release/lambda/$ARTEFACT/lambda.zip" "./target/x86_64-unknown-linux-musl/release/lambda/$ARTEFACT/bootstrap"
}

rm -rf ./provisioning/cdk.out && \
  cross build --target x86_64-unknown-linux-musl --release --bins && \
  packageLambda "organization_create" && \
  packageLambda "member_create" && \
  npm --prefix ./provisioning run deploy "FerndeskSelfservice$STAGE" && \
  rm -rf ./target/x86_64-unknown-linux-musl/release/lambda