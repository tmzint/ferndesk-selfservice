import dynamodb = require('@aws-cdk/aws-dynamodb');
import sns = require('@aws-cdk/aws-sns');
import cdk = require('@aws-cdk/core');
import lambda = require('@aws-cdk/aws-lambda');
import {RetentionDays} from "@aws-cdk/aws-logs";
import {Stage} from "./util";
import {Duration} from "@aws-cdk/core";

export interface FerndeskSelfserviceStackProps{
  readonly stage: Stage;
  readonly account: string;
  readonly region: string;
}

export class FerndeskSelfserviceStack extends cdk.Stack {

  constructor(scope: cdk.App, id: string, props: FerndeskSelfserviceStackProps) {
    super(scope, id, deriveStackProps(props));

    const ferndeskSelfserviceTable = new dynamodb.Table(this, `Table${props.stage}`, {
      partitionKey: { name: 'pk', type: dynamodb.AttributeType.STRING },
      sortKey: { name: 'sk', type: dynamodb.AttributeType.STRING },
      timeToLiveAttribute: "ttl",
      billingMode: dynamodb.BillingMode.PAY_PER_REQUEST,
      serverSideEncryption: true,
    });

    const ferndeskSelfserviceSnsTopic = new sns.Topic(this, `SnsTopic${props.stage}`, {});


    this.provisionOrganizationCreate(ferndeskSelfserviceTable, ferndeskSelfserviceSnsTopic, props);
    this.provisionMemberCreate(ferndeskSelfserviceTable, ferndeskSelfserviceSnsTopic, props);
  }

  provisionOrganizationCreate(ferndeskSelfserviceTable: dynamodb.Table, ferndeskSelfserviceSnsTopic: sns.Topic, props: FerndeskSelfserviceStackProps) {
    const ferndeskOrganizationCreateLambda = new lambda.Function(this, `OrganizationCreate${props.stage}`, {
      runtime: lambda.Runtime.PROVIDED,
      handler: "OrganizationCreate",
      description: "Create a organization",
      code: lambda.Code.fromAsset("../target/x86_64-unknown-linux-musl/release/lambda/organization_create/lambda.zip"),
      environment: {
        STAGE: props.stage,
        SELFSERVICE_TABLE_NAME: ferndeskSelfserviceTable.tableName,
        SELFSERVICE_SNS_TOPIC_ARN: ferndeskSelfserviceSnsTopic.topicArn,
      },
      memorySize: 128,
      timeout: Duration.seconds(3),
      logRetention: RetentionDays.ONE_WEEK,
    });
    ferndeskSelfserviceTable.grantFullAccess(ferndeskOrganizationCreateLambda);
    ferndeskSelfserviceSnsTopic.grantPublish(ferndeskOrganizationCreateLambda);
  }

  provisionMemberCreate(ferndeskSelfserviceTable: dynamodb.Table, ferndeskSelfserviceSnsTopic: sns.Topic, props: FerndeskSelfserviceStackProps) {
    const ferndeskMemberCreateLambda = new lambda.Function(this, `MemberCreate${props.stage}`, {
      runtime: lambda.Runtime.PROVIDED,
      handler: "MemberCreate",
      description: "Create a member",
      code: lambda.Code.fromAsset("../target/x86_64-unknown-linux-musl/release/lambda/member_create/lambda.zip"),
      environment: {
        STAGE: props.stage,
        SELFSERVICE_TABLE_NAME: ferndeskSelfserviceTable.tableName,
        SELFSERVICE_SNS_TOPIC_ARN: ferndeskSelfserviceSnsTopic.topicArn,
      },
      memorySize: 128,
      timeout: Duration.seconds(3),
      logRetention: RetentionDays.ONE_WEEK,
    });
    ferndeskSelfserviceTable.grantFullAccess(ferndeskMemberCreateLambda);
    ferndeskSelfserviceSnsTopic.grantPublish(ferndeskMemberCreateLambda);
  }

}

function deriveStackProps(props: FerndeskSelfserviceStackProps): cdk.StackProps {
  return {env: { region: props.region, account: props.account}};
}