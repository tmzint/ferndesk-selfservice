#!/usr/bin/env node
import cdk = require('@aws-cdk/core');
import { FerndeskSelfserviceStack } from '../lib/ferndesk-selfservice-stack';
import {Stage} from "../lib/util";

const app = new cdk.App();

const account = "070698996525";
const region = "eu-west-1";

const stages: Stage[] = ["Dev", "Int", "Prd"];

stages.forEach((stage) => {
    new FerndeskSelfserviceStack(app, `FerndeskSelfservice${stage}`, {
        stage: stage,
        account: account,
        region: region,
    });
});